import re
import argparse
import hashlib
import json
from xml.dom import minidom

level_map = {
    "1": "major",
    "2": "minor",
    "3": "info",
    "4": "info",
    "5": "info"
}


parser = argparse.ArgumentParser(description='Migrate roslint output into xml file.')
parser.add_argument('input', metavar='input', type=str, help='The input file')
parser.add_argument('output', metavar='output', type=str, help='The output file')
parser.add_argument('prefix', metavar='prefix', type=str, help='Prefix that should be stripped from the file path')
parser.add_argument('newprefix', metavar='newprefix', type=str, help='Prefix that should be added to the file path')

args = parser.parse_args()

pattern = re.compile("(?P<file>.*):(?P<line>\d+):  (?P<message>.*)  \[(?P<rule>.*)\] \[(?P<level>\d+)\]")
pattern_python = re.compile("(?P<file>.*):(?P<line>\d+):(?P<char>\d+): (?P<rule>[^ ]*) (?P<message>.*)")


findings = []

prefix = args.prefix
if not prefix.endswith('/'):
    prefix = prefix + '/'

xmlDoc = minidom.parse(args.input)
for failure in xmlDoc.getElementsByTagName('failure'):
    for line in failure.firstChild.data.splitlines():
        #print(line)
        for match in re.finditer(pattern, line):
            file = match.group('file')
            if file.startswith(prefix):
                file = file[len(prefix):]
            file = args.newprefix + file

            fingerprint = (file + ':' + match.group('line') + ':' + match.group('rule')).encode('utf-8')
            
            level = match.group('level')
            severity = level_map.get(level, 'unknown')
            print(severity)

            finding = {
                "description": match.group('rule') + ': ' + match.group('message'),
                "fingerprint": hashlib.md5(fingerprint).hexdigest(),
                "location": {
                    "path": file,
                    "lines": {
                        "begin": match.group('line'),
                    }
                },
                "severity": severity
            }
            findings.append(finding)

        for match in re.finditer(pattern_python, line):
            print(match.group('rule'))
            file = match.group('file')
            if file.startswith(prefix):
                file = file[len(prefix):]
            file = args.newprefix + file

            fingerprint = (file + ':' + match.group('line') + ':' + match.group('rule')).encode('utf-8')

            severity = 'unknown'

            finding = {
                "description": match.group('rule') + ': ' + match.group('message'),
                "fingerprint": hashlib.md5(fingerprint).hexdigest(),
                "location": {
                    "path": file,
                    "lines": {
                        "begin": match.group('line'),
                    }
                },
                "severity": severity
            }
            findings.append(finding)


with open(args.output, 'w') as outfile:
    json.dump(findings, outfile)