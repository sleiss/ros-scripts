import argparse
import hashlib
import json
import os


package_to_directory = {}

def parse_entry(entry, severity):
    if 'file' not in entry['location']:
        return None
    file = entry['location']['file']

    line = 0
    if 'line' in entry['location']:
        line = entry['location']['line']

    directory = ""
    if 'package' in entry['location']:
        package = entry['location']['package']
        directory = package_to_directory.get(package)
        if directory is None:
            # Use grep to find location of package.xml that contains this package
            stream = os.popen('grep "<name>' + entry['location']['package'] + '</name>" . -r -l')
            package_xml = stream.read()
            directory = package_xml[:-12] # Remove 'package.xml'
            package_to_directory[package] = directory
            print(directory)
    
    file = directory + file
        
    message = entry['text']
    rule = entry['id']

    fingerprint = (file + ':' + str(line) + ':' + rule).encode('utf-8')

    finding = {
        "description": message,
        "fingerprint": hashlib.md5(fingerprint).hexdigest(),
        "location": {
            "path": file,
            "lines": {
                "begin": line
            }
        },
        "severity": severity
    }
    return finding


parser = argparse.ArgumentParser(description='Migrate catkin lint output into json file.')
parser.add_argument('input', metavar='input', type=str, help='The input file')
parser.add_argument('output', metavar='output', type=str, help='The output file')

args = parser.parse_args()

findings = []


with open(args.input) as f:
    data = json.load(f)

findings = []

for error in data['errors']:
    parsed = parse_entry(error, 'major')
    if parsed is not None:
        findings.append(parsed)

for error in data['notices']:
    parsed = parse_entry(error, 'info')
    if parsed is not None:
        findings.append(parsed)

for error in data['warnings']:
    parsed = parse_entry(error, 'minor')
    if parsed is not None:
        findings.append(parsed)

print(findings)

with open(args.output, 'w') as outfile:
    json.dump(findings, outfile)