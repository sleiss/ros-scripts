import argparse
import json

parser = argparse.ArgumentParser(description='Migrate roslint output into xml file.')
parser.add_argument('inputs', metavar='inputs', nargs="+", type=str, help='The input file')
parser.add_argument('output', metavar='output', type=str, help='The output file')

args = parser.parse_args()


findings = []

for input in args.inputs:
    with open(input) as f:
        entries = json.load(f)
    for entry in entries:
        findings.append(entry)



with open(args.output, 'w') as outfile:
    json.dump(findings, outfile)